% !TeX root = main.tex

Neste capítulo, introduzimos os principais conceitos utilizados no texto.

\section{NP-difícil}

Na Teoria da Computação, um algoritmo é chamado de \textit{eficiente} se resolve um determinado problema em tempo polinomial no tamanho da representação da entrada. Um problema é chamado de \textit{Problema de Decisão} quando a solução consiste em responder ``sim'' ou ``não''. A classe de todos os problemas de decisão que podem ser resolvidos em tempo polinomial é chamada de~P e chamamos de~NP a classe de problemas de decisão que podemos verificar se a resposta é ``sim'' em tempo polinomial, isto é, dado um certificado de tamanho polinomial, podemos verificar em tempo polinomial se a resposta é ``sim''. A grande questão em aberto da Teoria da Computação é determinar se P~=~NP.

Chamamos de NP-difíceis os problemas que são tão difíceis quanto qualquer problema em NP, o que significa que qualquer problema em NP pode ser reduzido em tempo polinomial para um problema que é NP-difícil. Como MAXSPACE e MINSPACE são NP-difíceis~\cite{kumar2006scheduling}, não existe algoritmo para resolvê-los de forma eficiente, a menos que~P~=~NP~\cite{meurant2014algorithms}.

\section{Algoritmos Exatos}
No contexto de algoritmos exatos, procuramos desenvolver algoritmos para encontrar uma solução ótima. Em problemas de otimização combinatória existe, em geral, um número muito grande de soluções viáveis e enumerá-las pode tomar muito tempo. Por isso, é importante projetar um algoritmo que encontre soluções ótimas mais rapidamente.

%Esse tipo de algoritmo pode exigir que todas as soluções viáveis sejam verificadas, e resulta em um alto custo de tempo e processamento, o que muitas vezes torna inviável a busca por soluções exatas~\cite{fomin2010exact}.

A Programação Linear~(PL) é uma forma de solucionar problemas de otimização com restrições e função objetivo lineares. As restrições lineares definem um politopo convexo, que contém o conjunto de pontos viáveis. Uma das principais abordagens para projetar algoritmos exatos é a Programação Linear Inteira~(PLI), em que um problema é formulado usando Programação Linear com a restrição adicional de que todas ou algumas variáveis assumam valores inteiros~\cite{alves1997programaccao}. Em PLI podemos usar métodos exatos como o \textit{Branch and Bound}, o \textit{Branch and Price}, o \textit{Branch and Cut} e o Plano de Corte.

Algoritmos exatos também podem ser baseados em programação dinâmica e enumeração (usando o método \textit{Branch and Bound}, por exemplo), entre outros. O método \textit{Branch and Bound} consiste na enumeração de soluções candidatas, através da qual é possível eliminar subconjuntos de soluções infrutíferas, usando cálculos de limites superiores e inferiores. Já a programação dinâmica pode ser aplicada quando a solução ótima de um problema pode ser calculada a partir de soluções para problemas menores, que sobrepostos compõem o problema original.

\section{Heurísticas}

Com \textit{heurísticas} estamos interessados em encontrar soluções boas o suficiente, mas não necessariamente ótimas. Um algoritmo heurístico pode encontrar uma solução cujo valor é distante do valor da solução ótima, porém, para alguns problemas práticos, uma heurística pode encontrar soluções boas o suficiente e com a vantagem de calculá-las rapidamente. Em geral, heurísticas são interessantes pelas limitações encontradas para resolver um problema de forma exata, tais como limitação de memória e tempo para encontrar uma solução ótima.

Já as \textit{meta-heurísticas} são heurísticas de alto nível, que não foram desenvolvidas para um problema específico, mas que podem ser aplicadas a vários problemas~\cite{glover1986future}. Algumas das principais meta-heurísticas são as de Busca Local, Busca Tabu, GRASP, BRKGA, VNS e algoritmos genéticos~\cite{davis1991handbook, feo1995greedy, gendreau2010handbook, gonccalves2011biased, hansen2003variable}.

\subsection{Busca Local}\label{sec:ls}

A Busca Local começa com uma solução inicial e consiste em mover-se para soluções vizinhas à procura de melhoria. Quando nenhum movimento melhora a solução, significa que encontramos um ótimo local. Definimos como~$\caln(S)$ a vizinhança da solução~$S$ considerando a estrutura de vizinhança utilizada na Busca Local e como~$f(S)$ o valor da solução~$S$.

As estratégias \textit{Best Improvement} e \textit{First Improvement} são exemplos de buscas locais e são apresentados a seguir.

\begin{scriptsize}
\begin{algorithm}[H]
\caption{Exemplo de busca local com \textit{Best Improvement}}
\label{alg:bi}
\begin{algorithmic}[1]

\Procedure{BestImprovement}{$S$}
    \Repeat
        \State $S' \gets S$
        \State $S \gets \argmax_{S'' \in \caln(S)} f(S'')$  \Comment{Melhor vizinho em $\caln(S)$}
    \Until{$f(S) \leq f(S')$}
    \State \Return $S'$
\EndProcedure

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

No Algoritmo~\ref{alg:bi}, temos um exemplo de Busca Local usando o a estratégia \textit{best improvement}, em que a cada passo a busca move para o melhor vizinho da solução atual. A entrada do algoritmo é uma solução inicial~$S$ e a busca é executada até que o melhor vizinho possua valor menor ou igual ao da melhor solução encontrada. O algoritmo devolve a última solução que proporcionou melhoria.  Denotamos por~$S^i$ a~$i$-ésima solução de~$\caln(S)$. 

\begin{scriptsize}
\begin{algorithm}[H]
\caption{Exemplo de Busca Local com \textit{First Improvement}.}
\label{alg:fi}
\begin{algorithmic}[1]

\Procedure{FirstImprovement}{$S$}
    \Repeat
        \State $S' \gets S$
        \State $i \gets 0$
        \Repeat
            \State $i \gets i + 1$
            \State $S \gets \argmax\{f(S), f(S^i)\} $
        \Until{$f(S) > f(S')$ \textbf{ou} $i = |\caln(S)|$}
    \Until{$f(S) \leq f(S')$}
    \State \Return $S'$
\EndProcedure

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

No Algoritmo~\ref{alg:fi}, temos um exemplo de Busca Local usando o método \textit{first improvement}, em que a cada passo a busca move para o primeiro vizinho da solução atual que proporciona uma melhoria no valor da solução. A entrada do algoritmo é uma solução inicial~$S$. A cada iteração a melhor solução encontrada é mantida em~$S'$ e a busca é executada até que nenhum vizinho de~$S'$ possua valor maior que~$f(S')$. O algoritmo devolve a última solução que proporcionou melhoria.
%\begin{itemize}
    %\item Definir Busca Local
    %\item Apresentar o funcionamento básico da Busca Local
%\end{itemize}

\subsection{Busca Tabu}

A Busca Tabu é uma meta-heurística proposta por \citeauthor{glover1986future}~\cite{glover1986future} para permitir que a Busca Local supere ótimos locais. A ideia central da Busca Tabu é memorizar os movimentos de melhoria realizados pela Busca Local numa estrutura chamada Lista Tabu e proibir que esses movimentos sejam realizados enquanto estiverem nessa lista. Cada elemento permanece na Lista Tabu até que uma determinada quantidade de melhorias sejam realizadas~\cite{gendreau2010handbook}.

\begin{scriptsize}
\begin{algorithm}[H]
\caption{Pseudocódigo da Busca Tabu.}
\label{alg:ts}
\begin{algorithmic}[1]

\Procedure{BuscaTabu}{$S$, $maxTabuSize$}
    \State $S' \gets S$, $c_{max} \gets S$
    \State $tabu \gets \{S\}$
    \While{não atingir a condição de parada}
        \State $\caln \gets $ vizinhança de $c_{max}$
        \For{\textbf{cada} $c \in \caln$}
            \If{$c \not \in tabu$ e $f(c) > f(c_{max})$}
                \State $c_{max} \gets c$
            \EndIf
        \EndFor
        \If{$f(c_{max}) > f(S')$}
            \State $S' \gets c_{max}$
        \EndIf
        \State $tabu \gets tabu \cup \{c_{max}\}$
        \If{$|tabu| > maxTabuSize$}
            \State remove o primeiro elemento de $tabu$
        \EndIf
    \EndWhile
    \State \Return $S'$
\EndProcedure

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

O Algoritmo~\ref{alg:ts} apresenta um pseudocódigo da Busca Tabu. Esse algoritmo recebe como entrada uma solução inicial~$S$ e o tamanho máximo da Lista Tabu~$maxTabuSize$. O algoritmo executa enquanto não atingir uma determinada condição de parada (por exemplo, uma quantidade de iterações). A cada iteração do algoritmo, a vizinhança~$\caln$ do melhor candidato encontrado~$c_{max}$ é explorada para encontrar o melhor vizinho~${c \in \caln}$ que não está na Lista Tabu. Esse melhor vizinho então se torna o novo melhor candidato. Caso~$c_{max}$ possua valor maior que~$f(S')$, atualizamos a melhor solução~$S'$ e adicionamos~$c_{max}$ à Lista Tabu, removendo o primeiro elemento adicionado na lista quando esta excede a capacidade máxima. Ao final, o algoritmo devolve a melhor solução encontrada~$S'$.

\subsection{Variable Neighborhood Search}

A \textit{Variable Neighborhood Search}~(VNS) é uma meta-heurística proposta por \citeauthor{mladenovic1997variable}~\cite{mladenovic1997variable} para resolver problemas de otimização. Ela consiste em uma \textit{fase descendente} com trocas sistemáticas de vizinhança para encontrar um ótimo local e uma \textit{fase de perturbação} para sair de \textit{vales}. Podemos definir~${\caln_k}$ (para~${k=1, \dots, k_{max}}$) como um conjunto de estruturas de vizinhanças para um dado problema e~$\caln_k(S)$ como o conjunto de soluções na~$k$-ésima vizinhança da solução~$S$~\cite{gendreau2010handbook}.

A VNS básico utiliza uma busca local (Linha~\ref{vns:bl} do Algoritmo~\ref{alg:vns}) como sub-rotina para a fase descendente, a sub-rotina~$\Call{Shake}{}$ (Linha~\ref{vns:shake} do Algoritmo~\ref{alg:vns}) para a fase de perturbação e a sub-rotina~$\Call{NeighborhoodChange}{}$ (Linha~\ref{vns:nc} do Algoritmo~\ref{alg:vns}) que define o processo de mudança de vizinhança. As sub-rotinas $\Call{Shake}{}$ e o $\Call{NeighborhoodChange}{}$ são apresentadas a seguir.

\begin{scriptsize}
\begin{algorithm}[H]
\caption{Fase de perturbação (\textit{Shaking})~\cite{gendreau2010handbook}.}
\label{alg:shake}
\begin{algorithmic}[1]

\Function{Shake}{$S$, $k$}
    \State $r \gets [1 + \Call{Rand}{0, 1}\times|\caln_k(S)|]$
    \State \Return a $r$-ésima solução em~$\caln_k(S)$
\EndFunction

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

O Algoritmo~\ref{alg:shake} apresenta o funcionamento da perturbação da VNS. Esse algoritmo recebe como parâmetro uma solução~$S$ e um valor~$k$ e escolhe aleatoriamente uma solução na~$k$-ésima vizinhança de~$S$ (denotada por~$\caln_k(S)$).

\begin{scriptsize}
\begin{algorithm}[H]
\caption{Mudança de vizinhança~\cite{gendreau2010handbook}.}
\label{alg:nc}
\begin{algorithmic}[1]

\Function{NeighborhoodChange}{$S$, $S'$, $k$}
    \If{$f(S) < f(S')$}
        \State $S \gets S'$
        \State $k \gets 1$
    \Else
        \State $k \gets k+1$
    \EndIf
    \State \Return $S, k$
\EndFunction

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

O Algoritmo~\ref{alg:nc} apresenta o funcionamento da mudança de vizinhança da VNS. Esse algoritmo recebe como entrada a melhor solução~$S$ encontrada até o momento, a solução atual~$S'$ e um inteiro~$k$, que indica qual a estrutura de vizinhança atual. Seja~$f(S)$ a função de valoração de uma solução e assumindo que o problema é de maximização, se~$S'$ possuir um valor maior que~$S$,~$S$ recebe~$S'$ e reiniciamos o algoritmo, alterando a estrutura de vizinhança atual~$k$ para~$1$. Caso contrário, incrementamos a vizinhança. O algoritmo devolve a nova melhor solução~$S$ e o novo valor de~$k$.

\begin{scriptsize}
\begin{algorithm}[H]
\caption{\textit{Basic VNS}~\cite{gendreau2010handbook}.}
\label{alg:vns}
\begin{algorithmic}[1]

\Procedure{BVNS}{$S$, $k_{max}$}
    \State $k \gets 1$
    \While{$k \le k_{max}$}
        \State $S' \gets \Call{Shake}{S, k}$\label{vns:shake}
        \State $S'' \gets \Call{BestImprovement}{S'}$\label{vns:bl}
        \State $S, k \gets \Call{NeighborhoodChange}{S, S'', k}$\label{vns:nc}
    \EndWhile
    \State \Return $S$
\EndProcedure

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

O Algoritmo~\ref{alg:vns} apresenta o funcionamento da VNS básico, que recebe como entrada uma solução inicial~$S$ e um valor inteiro~$k_{max}$ que define a quantidade de estruturas de vizinhanças que são consideradas. A cada iteração a estrutura de vizinhança~$k$ é considerada, selecionando aleatoriamente um vizinho em~$\caln_k(S)$ para ser explorado pela busca local~($\Call{BestImprovement}{}$), que é executada em todas as vizinhanças consideradas. Em seguida a sub-rotina~$\Call{NeighborhoodChange}{}$ é executada para realizar a mudança de vizinhança. Após~$k_{max}$ iterações seguidas sem melhoria, o algoritmo é finalizado e devolve a melhor solução encontrada.

A fase de busca local também pode ser substituída pela \textit{Variable Neighborhood Descent}, apresentado na Seção~\ref{sec:vnd}.

%\begin{itemize}
    %\item Definir VNS
    %\item Apresentar o funcionamento básico do VNS
    %\item Apresentar o \textit{shaking}
%\end{itemize}

\subsection{Variable Neighborhood Descent}
\label{sec:vnd}

A \textit{Variable Neighborhood Descent}~(VND) é uma versão da VNS em que as vizinhanças são exploradas de forma determinística, escolhendo sempre o melhor vizinho a cada iteração~\cite{gendreau2010handbook}. A solução devolvida pela VND é ótimo local em todas as estruturas de vizinhança, caso contrário seria possível melhorá-la e o algoritmo reiniciaria. Um pseudocódigo da VND é apresentado no Algoritmo~\ref{alg:vnd}.


\begin{scriptsize}
\begin{algorithm}[H]
\caption{Pseudocódigo do VND~\cite{gendreau2010handbook}.}
\label{alg:vnd}
\begin{algorithmic}[1]

\Procedure{VND}{$S$, $k_{max}$}
    \State $k \gets 1$
    \While{$k < k_{max}$}
        \State $S' \gets \argmax_{S'' \in \caln_k(S)} f(S'')$  \Comment{Melhor vizinho em $\caln_k(S)$}
        \State $S, k \gets \Call{NeighborhoodChange}{S, S', k}$
    \EndWhile
    \State \Return $S$
\EndProcedure

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

A VND pode ser utilizada como meta-heurística principal para a resolução de problemas e como procedimento interno da VNS (na fase de busca local). A versão da VNS que utiliza a VND como busca local é conhecida como \textit{General VNS}~(GVNS).

%\begin{itemize}
    %\item Definir VND
    %\item Apresentar o funcionamento básico do VND
%\end{itemize}

\subsection{GRASP}

O \textit{Greedy Randomized Adaptive Search Procedure}~(GRASP) é uma meta-heurística que consiste na geração de soluções iniciais aleatórias de boa qualidade e o uso de busca local para fazer melhorias. O GRASP executa~$k$ iterações e, a cada iteração, uma solução inicial~$S'$ é gerada de forma aleatória e uma busca local é executada em~$S'$ para obter~$S''$. O GRASP devolve a melhor solução~$S$ encontrada durante as~$k$ iterações. Um pseudocódigo para o GRASP é apresentado no Algoritmo~\ref{alg:grasp}.

\begin{scriptsize}
\begin{algorithm}[H]
\caption{Pseudocódigo do GRASP~\cite{gendreau2010handbook}.}
\label{alg:grasp}
\begin{algorithmic}[1]

\Procedure{GRASP}{$it$, $\alpha$}
    \State $S \gets \emptyset$
    \For{$i \gets 1, \dots, k$}
        \State $S' \gets \Call{HeuristicaDeConstrucao}{\alpha}$
        \State $S'' \gets \Call{BuscaLocal}{S'}$
        \State $S \gets \max\{S, S''\}$
    \EndFor
    \State \Return $S$
\EndProcedure

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

As soluções iniciais do GRASP são geradas a partir do procedimento do Algoritmo~\ref{alg:grc}. A heurística de construção recebe um parâmetro~$\alpha$ que possui valor no intervalo~$[0, 1]$ que indica quão gulosa ou aleatória será. Quanto mais próximo de~$0$ é o valor de~$\alpha$, mais gulosa é a heurística de construção e quanto mais próximo de~$1$, mais aleatória. A cada iteração, essa sub-rotina seleciona os candidatos e calcula o valor de cada um em relação à solução em construção~$S$. Os elementos candidatos são aqueles que não estão na solução em construção~$S$ e que podem ser adicionados a~$S$. A partir da lista de candidatos e dos valores calculados, o algoritmo cria uma lista restrita de candidatos~$RC$, em que estarão apenas os melhores candidatos, isto é, sejam~$max$ e~$min$, respectivamente, o maior e o menor valor dentre os candidatos, um elemento será adicionado à~$RC$ se o seu valor estiver no intervalo~${[max-\alpha(max-min), max]}$. Após a criação da lista restrita de candidatos, um elemento de~$RC$ é escolhido de forma uniformemente aleatória para fazer parte de~$S$ e o procedimento se repete. O algoritmo termina quando não há mais candidatos e a solução~$S$ é devolvida.

\begin{scriptsize}
\begin{algorithm}[H]
\caption{Pseudocódigo da Heurística de Construção~\cite{gendreau2010handbook}.}
\label{alg:grc}
\begin{algorithmic}[1]

\Procedure{HeuristicaDeConstrucao}{$\alpha$}
    \State $S \gets \emptyset$
    \State $C \gets $ conjunto de elementos candidatos
    \State Calcule o valor de cada candidato
    \While{$C \neq \emptyset$}
        \State $RC \gets $ lista restrita de candidatos
        \State Escolha aleatoriamente um elemento $s$ de $RC$
        \State $S \gets S \cup \{s\}$
        \State atualize o conjunto $C$ e os valores dos candidatos
    \EndWhile
    \State \Return $S$
\EndProcedure

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

\section{Algoritmos de Aproximação}

Muitas vezes, na prática, é suficiente encontrar soluções com valor próximo ao valor da solução ótima e que sejam calculadas de forma eficiente. Um \textit{Algoritmo de Aproximação} encontra, em tempo polinomial, uma solução viável que possui valor garantidamente próximo da solução ótima~\cite{vazirani2013approximation}.

Considere um problema de otimização~$\Pi$ com função objetivo~$f_{\Pi}$. Quando~$\Pi$ é um problema de maximização, dizemos que um algoritmo~$H$ é uma~$\alpha$-aproximação para~$\Pi$ se para qualquer instância~$I$ de~$\Pi$ ele consome tempo polinomial no tamanho da representação binária de~$I$ e produz uma solução~$S$ tal que~${f_{\Pi}(S) \ge \alpha\cdot \OPT}$, com~${\alpha \leq 1}$ e em que~$\OPT$ é o valor da solução ótima de~$I$. Quando temos um problema~$\Pi$ de minimização, uma~$\alpha$-aproximação~$H$ é tal que~$H$ é um algoritmo polinomial no tamanho da representação binária da entrada~$I$ e~${f_{\Pi}(S) \leq \alpha\cdot \OPT}$, para~${\alpha \geq 1}$.

Dizemos que~$H$ é um esquema de aproximação para~$\Pi$ se para qualquer instância~$I$ de~$\Pi$ e dado um parâmetro de erro~${\varepsilon > 0}$,~$H$ devolve uma solução~$S$ tal que~${f_{\Pi}(S) \leq (1+\varepsilon)\OPT}$ se~$\Pi$ é um problema de minimização e~${f_{\Pi}(S) \geq (1-\varepsilon)\OPT}$ se~$\Pi$ é um problema de maximização, em que~$\OPT$ é o valor da solução ótima de~$I$~\cite{vazirani2013approximation}.

Um \textit{Polynomial-Time Approximation Scheme}~(PTAS) é um esquema de aproximação que é polinomial no tamanho da instância mas não necessariamente em~$1/\varepsilon$. Já um \textit{Fully Polynomial-Time Approximation Scheme}~(FPTAS) é um esquema de aproximação que executa em tempo polinomial no tamanho da instância e em~$1/\varepsilon$~\cite{vazirani2013approximation}.