% !TeX root = main.tex

Neste capítulo, apresentamos as meta-heurísticas aplicadas ao MAXSPACE e à variante MAXSPACE-RDWV.

\section{Vizinhanças}
\label{sec:neigh}

As meta-heurísticas aplicadas neste trabalho são baseadas em busca local. A seguir, apresentamos as estruturas de vizinhanças consideradas nos algoritmos de busca local desenvolvidos.

ADD$(A_i)$: adiciona a propaganda~$A_i$ à solução atual. Cada movimento nessa vizinhança representa uma propaganda que pode ser adicionada na solução, isto é, é possível adicionar as~$w_i$ (ou pelo menos~$w^{min}_{i}$, no caso do MAXSPACE-RDWV) cópias de~$A_i$ na solução mantendo a viabilidade. A propaganda é disposta usando a heurística \textit{first-fit}, que adiciona cada cópia ao primeiro \textit{slot} que não possua cópia de~$A_i$, que possua espaço suficiente e que, no caso da variante, não viola as restrições de \textit{release date} e \textit{deadline}. No MAXSPACE-RDWV, ADD$(A_i)$ irá inserir o maior número de cópias possível de~$A_i$, sem exceder~$w^{max}_{i}$.

Na implementação dessa vizinhança para o MAXSPACE-RDWV, utilizamos uma Árvore de Indexação Binária~(BIT) introduzida por Fenwick~\cite{fenwick1994new}, com o objetivo de acelerar algumas verificações. Essa árvore permite verificar a soma de um determinado intervalo em um vetor de~$d$ inteiros em tempo~$O(\lg{d})$. A BIT também pode ser implementada de forma que também permita verificar em tempo~$O(\lg{d})$ qual o maior ou menor valor de um intervalo no vetor, como apresentado em~\citeauthor{dima2015efficient}~\cite{dima2015efficient}. O custo de construção da BIT é~$O(d)$ e de atualização é~$O(\lg{d})$.

Utilizamos essa árvore para verificar o espaço vazio de um intervalo de \textit{slots}. Por exemplo, se queremos inserir uma propaganda~$A_i$, verificamos na BIT em~$O(\lg{K})$ se o espaço vazio no intervalo de \textit{slots}~${[r_i, d_i]}$ é pelo menos~$w_is_i$. Também verificamos em~$O(\lg{K})$ se o \textit{slot} menos cheio de um dado intervalo possui espaço suficiente para uma cópia de~$A_i$.

CHG${(A_i, A_j)}$: remove a propaganda~$A_i$ que já está na solução e adiciona a propaganda~$A_j$ que não está na solução. Nessa estrutura de vizinhança são consideradas apenas trocas válidas, ou seja, quando é possível adicionar~$A_j$ na solução atual após a remoção de~$A_i$. A adição da propaganda~$A_j$ é feita da mesma forma que em~ADD. O custo de gerar toda a vizinhança de troca de uma solução é alto (inclusive usando BIT).
%\lehi{não entendi; você não usou chg? então pq descrevê-la?}

%Na implementação dessa est rutura de vizinhança para o MAXSPACE-RDWV, utilizamos a BIT para verificar se remover uma propaganda~$A_i$ permite que uma propaganda~$A_j$ possa ser adicionada. A ideia é acelerar a verificação em casos que não há espaço suficiente para~$A_j$ ser disposta, ainda que a propaganda~$A_i$ seja removida.

RPCK${(A_i^l, A_j^k)}$: troca a~$l$-ésima cópia de uma propaganda~$A_i$ na solução pela~$k$-ésima cópia de uma propaganda~$A_j$ que também está na solução. Com essa vizinhança queremos reorganizar (\textit{repack}) as cópias das propagandas que já estão alocadas em uma determinada solução. O objetivo é abrir espaço para que novas propagandas ou novas cópias de propagandas possam ser adicionadas, porém isso não altera o valor da solução. Assim, utilizamos a métrica de minimizar a soma do quadrado do espaço vazio nos \textit{slots}, isto é, seja~$Area(B_j)$ o preenchimento de um \textit{slot}~$j$, queremos minimizar~$\sum^{K}_{j=1}{(L - Area(B_j))^2}$. Com essa métrica queremos nivelar o preenchimento dos \textit{slots}, ou seja, deixar \textit{slots} com preenchimentos mais parecidos, com isso evitamos que existam \textit{slots} muito cheios quando as propagandas poderiam ser distribuídas para \textit{slots} que estão mais vazios. Nessa vizinhança são consideradas apenas as mudanças que mantêm a viabilidade da solução.

ADDCPY${(A_i, k, j)}$: adiciona a~$k$-ésima cópia de uma propaganda~$A_i$ que já está na solução ao \textit{slot}~$j$. Essa estrutura de vizinhança faz sentido apenas quando pensamos no MAXSPACE-RDWV, em que uma propaganda possui uma frequência entre~$w^{min}_{i}$ e~$w^{max}_{i}$. A ideia é tentar adicionar mais uma cópia de uma propaganda que já possui pelo menos~$w^{min}_{i}$ cópias na solução atual e que não possui~$w^{max}_{i}$ cópias alocadas ainda. Além disso, a viabilidade da solução deve ser mantida.

MV${(A_i^l, j)}$: move a~$l$-ésima cópia de uma propaganda~$A_i$ na solução para o \textit{slot}~$j$. Assim como na vizinhança~RPCK, desejamos reorganizar as cópias das propagandas que já estão alocadas em uma determinada solução com o objetivo de abrir espaço para que novas propagandas ou novas cópias de propagandas possam ser adicionadas. Novamente utilizamos a métrica de minimizar a soma do quadrado do espaço vazio nos \textit{slots}, isto é, minimizar~$\sum^{K}_{j=1}{(L - Area(B_j))^2}$. Nessa vizinhança também são consideradas apenas as mudanças que mantêm a viabilidade da solução.

\section{Heurística de construção}
\label{sec:hc}
A construção de solução inicial foi desenvolvida considerando que o custo de cada propaganda~$A_i$ é~$s_iw_i$ no MAXSPACE e~$v_i/s_i$ no MAXSPACE-RDWV. A cada iteração da heurística de construção,~$max$ recebe o maior custo dentre as propagandas que ainda não foram adicionadas,~$min$ recebe o menor custo dentre as propagandas que ainda não foram adicionadas e, dado um~$\alpha$, uma propaganda~$A_j$ é escolhida de forma aleatória dentre as propagandas que possuem valor no intervalo~\mbox{$[max-\alpha(max-min), max]$}. A propaganda selecionada~$A_j$ é adicionada usando \textit{first-fit}, se couber, caso contrário~$A_j$ é descartada. A heurística de construção termina quando todas as propagandas forem consideradas. O Algoritmo~\ref{alg:hc} apresenta um pseudocódigo para essa heurística de construção.

\begin{scriptsize}
\begin{algorithm}[H]
\caption{pseudocódigo da heurística de construção inicial}
\label{alg:hc}
\begin{algorithmic}[1]

\Procedure{HeuristicaConstrucao}{$\alpha$}
    \State $S \gets \emptyset$
    \State $C \gets \cala$
    \For{$A_i \in C$}
        \State $custo(A_i) \gets s_iw_i$ \Comment{em MAXSPACE-RDWV o custo é $v_i/s_i$}
    \EndFor
    \While{$C \neq \emptyset$}
        \State $min \gets \min_{A_i \in C}\{custo(A_i)\}$
        \State $max \gets \max_{A_i \in C}\{custo(A_i)\}$
        \State $RC \gets \emptyset$
        \For{$A_i \in C$}
            \If{$custo(A_i) \geq max-\alpha(max-min)$}
                \State $RC \gets RC \cup \{A_i\}$
            \EndIf
        \EndFor
        \State Escolha aleatoriamente uma propaganda $A_j$ de $RC$
        \State \Call{FirstFit}{$A_j$, $S$}
        \State $C \gets C \setminus \{A_j\}$
    \EndWhile
    \State \Return $S$
\EndProcedure

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

\section{VNS}
\label{sec:vns}

Nesta seção, explicamos como foi aplicada a meta-heurística VNS ao problema MAXSPACE e ao MAXSPACE-RDWV. Para o MAXSPACE foram consideradas as seguintes estruturas de vizinhança e na seguinte ordem: MV, RPCK, ADD e CHG. Já para o MAXSPACE-RDWV, foram consideradas todas as estruturas, na seguinte ordem: MV, RPCK, ADDCPY, ADD e CHG. A ordem das vizinhanças foi definida levando em consideração o custo de calculá-las, deixando para o final as vizinhanças mais custosas, o que faz com que elas sejam exploradas menos vezes, dada a natureza de funcionamento do algoritmo.

A construção da solução inicial para da VNS foi feita utilizando a heurística de construção da Seção~\ref{sec:hc}. Na etapa de busca local foi utilizada a meta-heurística VND. Além disso, o~\textit{shaking} da VNS foi alterado para realizar~$\mathcal{K}_{max}$ perturbações antes de trocar de vizinhança, para aumentar a chance de o algoritmo fugir de mínimos locais.

\section{Busca Tabu}
\label{sec:ts}

A solução inicial da Busca Tabu é construída usando a heurística de construção da Seção~\ref{sec:hc}. A Busca Tabu implementada utilizou todas as estruturas de vizinhança apresentadas na Seção~\ref{sec:neigh}. O critério de parada utilizado foi a quantidade de iterações. A cada iteração da Busca Tabu, uma vizinhança é escolhida aleatoriamente para que um movimento seja realizado.

A Lista Tabu armazena apenas os movimentos realizados, dado que seria caro armazenar as soluções. Por exemplo, para um movimento do tipo~CHG${(A_i, A_j)}$, a lista armazena apenas o tipo do movimento e as propagandas~$A_i$ e~$A_j$.

\section{GRASP}
\label{sec:grasp}

Utilizamos a heurística de construção apresentada na Seção~\ref{sec:hc} para criar soluções inicias a cada iteração. Foram implementadas três versões do GRASP. A primeira utiliza uma busca local com \textit{best improvement}, a segunda versão utiliza a VNS (como apresentado na Seção~\ref{sec:vns}) como busca local e a terceira versão utiliza a Busca Tabu (como apresentada na Seção~\ref{sec:ts}) como processo de busca local.
