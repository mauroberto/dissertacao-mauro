#sudo apt-get install texlive-full

FILE=main

$(FILE).pdf: clear $(FILE).tex *.bib *.sty *.cls
	pdflatex $(FILE).tex
	bibtex $(FILE).aux
	pdflatex $(FILE).tex
	pdflatex $(FILE).tex
	
clear:
	rm -f $(FILE).pdf