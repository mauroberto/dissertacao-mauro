\section{PTAS para o MAXSPACE-R com K constante}
\label{ptasr}

Nesta seção, apresentamos um PTAS para o MAXSPACE-R com número de \textit{slots} constante. Nesse algoritmo, dada uma constante~${\varepsilon > 0}$, particionamos as propagandas por tamanho: grandes~${G = \{A_i \in \cala \mid s_i > \varepsilon\}}$ e pequenas~${P = \cala \setminus G}$.

%Denotamos por~$S$ uma solução viável~${\cala'\subseteq \cala}$ agendada em \textit{slots}~${B_1, B_2, \dots, B_K}$. O \textit{preenchimento} de um \textit{slot}~$B_j$ é definido como~${f(B_j) = \sum_{A_i \in B_j} s_i}$. Já o preenchimento de uma solução~$S$ é dado por~${f(S) = \sum_{j = 1}^{K}{f(B_j)}}$.

Uma \textit{configuração} para um subconjunto de propagandas~$\cala'$ é uma solução viável que dispõe todas as propagandas em~$\cala'$. O Lema~\ref{lemma:grandes} mostra que se~$K$ é constante, então o número de configurações possíveis contendo apenas propagandas grandes é polinomial no tamanho de~$G$ e podem ser enumeradas por um algoritmo de força bruta.

\begin{lemma}
    \label{lemma:grandes} %
    Se~$K$ é constante, então as configurações de~$G$ podem ser listadas em tempo polinomial.
\end{lemma}

\begin{proof}
Como cada \textit{slot} tem tamanho~$1$, o número de propagandas grandes que podem ser dispostas em um \textit{slot} é no máximo~${1/\varepsilon}$. Então, uma solução viável contém no máximo~${R = K(1/\varepsilon)}$ propagandas. Selecione cada subconjunto de~${G'\subseteq G}$ com~${|G'| \le R}$. Observe que o número de subconjuntos é no máximo~$O\left(\binom{|G|}{R}\right)$, o que é polinomial em~$|G|$, dado que~$R$ é constante.

Para cada~$G'$, crie um multiconjunto~$M$ em que cada propaganda~${A_i \in G'}$ aparece~$w_i$ vezes e liste cada partição de~$M$ em conjuntos~${B_1, B_2, \dots, B_K}$. Como~${|M| \le K R}$ é constante, o número de partições para cada~$G'$ é constante. Agora note que qualquer solução viável de~$G'$, se existir, é uma partição de~$M$. Isso completa a prova.
\end{proof}

\subsection{Algoritmo para Propagandas Pequenas}

O Lema~\ref{lemma:grandes} mostra que todas as configurações para propagandas grandes podem ser listadas em tempo polinomial e assim podemos encontrar a configuração das propagandas grandes de uma solução ótima. Isso sugere que a parte difícil do MAXSPACE-R é obter uma solução para propagandas pequenas, dada uma configuração para propagandas grandes. Nesta seção, apresentamos um algoritmo para propagandas pequenas e para a generalização do problema em que os \textit{slots} possuem tamanho variável, ou seja, cada \textit{slot}~$j$ possui tamanho~${T_j \leq L}$.

O Algoritmo~\ref{alg:6} tem parâmetro constante~${\varepsilon > 0}$ e recebe como entrada um conjunto de propagandas pequenas~$P$ e um vetor~$T$ de tamanhos, em que cada~$T_j$ é o tamanho do \textit{slot}~$B_j$. O funcionamento do Algoritmo~\ref{alg:6} é semelhante ao funcionamento do Algoritmo~\ref{alg:3}. Nos Lemas~\ref{poly:alg:6} e~\ref{feasible:alg:6}, mostraremos que o Algoritmo~\ref{alg:2} é polinomial no tamanho da instância e devolve uma solução viável. No Lema~\ref{lema:psv} mostraremos que se uma propaganda~$A_i$ pertencente a uma solução ótima não é disposta pelo algoritmo, então cada \textit{slot} de índice~$j$, tal que~$A_i$ está em~$B_j$ na solução ótima, possui preenchimento pelo menos~${T_j - 2\varepsilon}$.

\begin{scriptsize}
\linespread{1}% FIXME: REMOVER DEPOIS
\begin{algorithm}[H]
\caption{Algoritmo para propagandas pequenas com \textit{slots} de tamanhos variáveis}
\label{alg:6}
\begin{algorithmic}[1]


\Procedure{Alg\_PSV}{$P$, $T$}
    \For{$j \gets 1, \dots, K$}
        $B_j \gets \emptyset$
    \EndFor
    \For{\textbf{cada }$A_i \in P$ em ordem não-decrescente de $r_i$}
    \label{lin3:goto2_psv}
        \State $X \gets \emptyset$
        \For{$k \gets 1, \dots, w_i$}
            \If{existir $j \not \in X$ com $j \geq r_i$ e com $f(B_j) < T_j - 2\varepsilon$} \label{lin:casoa2_psv}
                \State $j \gets \min\{j' \mid j' \not \in X$, $j' \geq r_i$ e $f(B_{j'}) < T_j - 2\varepsilon\}$
                \State $X \gets X \cup \{j\}$
            \ElsIf{existir $j_1 \in X$ e $j_2 \not \in X$ com $f(B_{j_1}) < B_{j_1} - 2\varepsilon$, $f(B_{j_2}) \geq T_{j_2} - \varepsilon$, $j_1 \geq r_i$ e $j_2 \geq r_i$}\label{lin:casob2_psv}
                \State $j_1 \gets \min\{j' \mid j' \in X$, $j' \geq r_i$ e $f(B_{j'}) < T_{j'} - 2\varepsilon\}$
                \State $j_2 \gets \min\{j' \mid j' \not \in X$, $j' \geq r_i$ e $f(B_{j'}) \geq T_{j'} - \varepsilon\}$
                \State encontre $to_{j_1} \subset B_{j_2} \mid \varepsilon \leq f(to_{j_1}) \leq 2\varepsilon$ e $to_{j_1} \cap B_{j_1} = \emptyset$
                \State mova $to_{j_1}$ para $B_{j_1}$
                \State $X \gets X \cup \{j_2\}$
            \ElsIf{existir $j \not \in X$ com $f(B_j) \leq T_j - s_i$ e $j \geq r_i$}\label{lin:casoc2_psv}
                \State $j \gets \argmin \{f(B_{j'}) \mid j' \not \in X, j' \geq r_i\}$
                \State $X \gets X \cup \{j\}$
            \Else
                \State vá para a Linha~\ref{lin3:goto2_psv}
            \EndIf
        \EndFor
        \State adicione uma cópia de $A_i$ a $B_j$ para cada $j \in X$
    \EndFor
    \State \Return $\{B_1, B_2, \dots, B_K\}$
\EndProcedure

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

\begin{lemma}
    \label{poly:alg:6}
    O Algoritmo~\ref{alg:6} é polinomial no tamanho da instância.
\end{lemma}

\begin{proof}
    A ordenação das propagandas por \textit{release date} é executada em tempo polinomial e o laço da Linha~\ref{lin3:goto2_psv} também. Os \textit{slots} aos quais as propagandas serão adicionadas podem ser encontrados em~$O(K)$ e a troca de propagandas de \textit{slots} pode ser feita em~$O(K|P|)$. A complexidade do algoritmo é~${O(K|P|\sum_{A_i \in P}{w_i}+|P|\lg|P|)}$, que é polinomial.
\end{proof}

\begin{lemma}
    \label{feasible:alg:6}
    O Algoritmo~\ref{alg:6} devolve uma solução viável.
\end{lemma}

\begin{proof}
    O algoritmo adiciona uma cópia de uma propaganda~$A_i$ apenas a \textit{slots} compatíveis. Além disso,~$A_i$ é adicionada apenas se exatamente~$w_i$ cópias de~$A_i$ podem ser adicionadas a \textit{slots} compatíveis. Ao mover um conjunto de propagandas de um \textit{slot}~$j_2$ para um \textit{slot}~$j_1$ o algoritmo não viola as restrições de \textit{release dates}, dado que as propagandas de~$to_{j_1}$ possuem \textit{release dates} menores ou iguais ao \textit{release date} considerado na iteração, pela ordem considerada das propagandas, e o algoritmo também não viola o preenchimento dos \textit{slots}, dado que~${f(B_{j_1}) < T_{j_1} - 2\varepsilon}$ e~${f(to_{j_1}) \leq 2\varepsilon}$. O conjunto de propagandas~${to_{j_1} \subset B_{j_2}}$ não está em~$j_1$, então a restrição de cada propaganda possuir no máximo uma cópia por \textit{slot} não é violada por esse movimento. Dessa forma, o algoritmo devolve uma solução viável.
\end{proof}

Considere a saída do Algoritmo~\ref{alg:6} e seja~${\OPT = \{B^*_1, B^*_2, \dots, B^*_K\}}$ uma disposição ótima. Também sejam~$H$ o conjunto de propagandas não dispostas pelo algoritmo e~$H^*$ o subconjunto de propagandas de~$H$ que estão em~$\OPT$.

\begin{lemma}
\label{lema:psv}
Seja~$T_j$ o tamanho total do \textit{slot}~$j$,~${A_i \in H^*}$ e~$Z$ o conjunto de índices~$j$ tais que~${r_i \leq j}$. Então para cada~${j \in Z}$, vale que~${f(B_j) \geq T_j - 2\varepsilon}$.
\end{lemma}

\begin{proof}
Considere~${B_1, \dots, B_K}$ no momento em que se tentou dispor a propaganda~$A_i$.
Considere o momento em que~$A_i$ foi descartada.
Como o caso da linha~\ref{lin:casoa2_psv} falhou, segue que todos os \textit{slots}~${Z \setminus X}$ tinham preenchimento pelo menos~${T_j - \varepsilon}$.
Como o caso da linha~\ref{lin:casoc2_psv} falhou, então existe pelo menos um \textit{slot}~${B_j \in Z}$ cuja área preenchida é maior que~${T_j - \varepsilon}$ (já que~${s_i \leq \varepsilon}$).
Como o caso da linha~\ref{lin:casob2_psv} falhou, segue que todos os \textit{slots} em~$X$, nesse momento, tinham preenchimento pelo menos~${T_j - 2\varepsilon}$. Observe que após movermos as propagandas de~$to_{j_1}$ os \textit{slots} não têm o seu preenchimento diminuído a menos de~${T_j - 2\varepsilon}$.
Concluímos que nesse momento todos os \textit{slots} em~${X' = (Z \setminus X) \cup X}$ tinham preenchimento pelo menos~${T_j - 2\varepsilon}$. Como~${Z \subseteq X'}$, o resultado vale.
\end{proof}

\subsection{PTAS para o caso geral}

Nesta seção, apresentamos um PTAS para MINSPACE-R com~$K$ constante.

\begin{scriptsize}
\begin{algorithm}[H]
\caption{Algoritmo do caso geral}
\label{alg:7}
\begin{algorithmic}[1]

\Procedure{Alg\_CG}{$\cala$}
    \State ${G = \{A_i \in \cala \mid s_i > \varepsilon\}}$
    \State ${P = \cala \setminus G}$
    \State $C \gets $ todas as configurações para propagandas grandes
    \State $S_{max} \gets $ \textit{slots} vazios
    \For{\textbf{cada} $c \in C$}
        \State $S_G \gets $ alocação das propagandas seguindo a configuração~$c$
        \State $T_j \gets $ espaço vazio no \textit{slot}~$B_j$ na solução~$S'$, para todo~$j$
        \State $S_P \gets \Call{Alg\_PSV}{P, T}$
        \State $S \gets S_G \cup S_P$
        \If{$f(S) > f(S_{max})$}
            \State ${S_{max} \gets S}$
        \EndIf
    \EndFor
    \State \Return $S_{max}$
\EndProcedure

\end{algorithmic}
\end{algorithm}
\end{scriptsize}

O Algoritmo~\ref{alg:7} tem como parâmetro uma constante~${\varepsilon > 0}$ e recebe como entrada um conjunto de propagandas~$\cala$ e particiona as propagandas em grandes~$G$ e pequenas~$P$. Pelo Lema~\ref{lemma:grandes}, é possível enumerar todas as configurações viáveis para propagandas de~$G$. Dessa forma, o algoritmo enumera cada uma das possíveis configurações~$G$ e utiliza o Algoritmo~\ref{alg:6} para adicionar as propagandas pequenas aos espaços restantes dos \textit{slots}.

Nos Lemas~\ref{poly:alg:7} e~\ref{feasible:alg:7}, mostraremos que o Algoritmo~\ref{alg:7} é polinomial no tamanho da instância e devolve uma solução viável. No Lema~\ref{ptas:alg:7}, mostraremos que o Algoritmo~\ref{alg:7} é um PTAS para o conjunto de propagandas~$\cala$.

\begin{lemma}
    \label{poly:alg:7}
    O Algoritmo~\ref{alg:7} é polinomial no tamanho da instância.
\end{lemma}

\begin{proof}
    Para cada configuração para propagandas grandes, o Algoritmo~\ref{alg:7} executa o Algoritmo~\ref{alg:6} para dispor as propagandas pequenas nos espaços restantes. Pelo Lema~\ref{lemma:grandes}, o número de configurações para as propagandas de~$G$ é polinomial e pelo Lema~\ref{poly:alg:6} o Algoritmo~\ref{alg:6} é polinomial. Logo o algoritmo é polinomial.
\end{proof}

\begin{lemma}
    \label{feasible:alg:7}
    O Algoritmo~\ref{alg:7} devolve uma solução viável.
\end{lemma}

\begin{proof}
    O algoritmo enumera apenas as configurações viáveis de~$G$ e, para cada uma, executa o Algoritmo~\ref{alg:6} para dispor as propagandas pequenas nos espaços restantes. Como o Algoritmo~\ref{alg:6} utiliza apenas os espaços restantes nos \textit{slots}, nenhum \textit{slot} possui a capacidade violada. Além disso, as demais restrições também são satisfeitas, pelo Lema~\ref{feasible:alg:6}. Logo o algoritmo devolve uma solução viável.
\end{proof}


\begin{lemma}
    \label{ptas:alg:7}
    O Algoritmo~\ref{alg:7} é um PTAS para o MAXSPACE-R com~$K$ constante.
\end{lemma}

\begin{proof}
    Pelos Lemas~\ref{poly:alg:7} e~\ref{feasible:alg:7}, o algoritmo é polinomial e devolve uma solução viável.

    Seja~$\OPT$ uma solução ótima para o MAXSPACE-R com~$K$ \textit{slots}. Essa solução pode ser decomposta na disposição de propagandas grandes~$\OPT_G$ e pequenas~$\OPT_P$, então~${f(\OPT) = f(\OPT_G) + f(\OPT_P)}$. Seja~$S$ a solução devolvida pelo algoritmo. Essa solução também pode ser decomposta na disposição para propagandas grandes~$S_G$ e pequenas~$S_P$, então~${f(S) = f(S_G) + f(S_P)}$. Sejam~${\{B^*_1, B^*_2, \dots, B^*_K\}}$ os \textit{slots} de~$\OPT$ e sejam~${\{B_1, B_2, \dots, B_K\}}$ os \textit{slots} de~$S$.

    Como todas as configurações para~$G$ são enumeradas e a melhor solução é devolvida, podemos supor sem perda de generalidade que~${S_G = \OPT_G}$ e~${f(\OPT_G) = f(S_G)}$. Se não existe~${A_i \in P}$ que está na solução ótima~$\OPT$ e que não foi alocada pelo algoritmo, então~${S_P = \OPT_P}$, logo o resultado vale. Sejam então~${\cala' \subseteq P}$ as propagandas que estão na solução ótima~$\OPT$, mas não foram alocadas pelo algoritmo. Seja~$l$ o \textit{slot} com menor índice tal que~${B^*_l \cap N^* \neq \emptyset}$ e seja~$A_i$ uma propaganda em~${B^*_l \cap N^*}$. Seja~$m$ o menor índice de \textit{slot} tal que~${f(B_j) \geq T_j - 2\varepsilon}$ para todo~${j \geq m}$. Seja~$Z$ os \textit{slots} com indíce menor que~$m$ e seja~$\overline{Z}$ o conjunto de \textit{slots} com índice maior ou igual a~$m$.

    Note que~${m \leq l}$ (pelo Lema~\ref{lema:psv}) e que isso implica que todo~${A_i \in B^*_j}$ com~${j \leq m}$ está em~$E^*$. Note também que~${\sum_{j \in Z}{f(B_j)} \geq \sum_{j \in Z}{f(B^*_j)}}$, pois, se existir~$A_i$ com menos cópias em~${\{ B_1, B_2, \dots, B_{m-1}\}}$ do que em~${\{ B^*_1, B^*_2, \dots, B^*_{m-1}\}}$, então existe um indíce de \textit{slot}~${e \leq m}$ tal que~${A_i \not \in B_e}$ e~${A_i \in B^*_e}$. Pelo preenchimento do algoritmo,~${f(B_j) \geq T_j - 2\varepsilon}$ para todo~${j \geq e}$, o que é uma contradição com a definição de~$m$. Logo,

    \begin{align*}
        f(S_P)
            & = \sum_j f(B_j) = \sum_{j \in Z} f(B_j) + \sum_{j \in \overline{Z}}{f(B_j)}\\
            & \geq \sum_{j \in Z} {f(B_j)} + \sum_{j \in \overline{Z}}{[T_j - 2\varepsilon]}\\
            & \geq \sum_{j \in Z} {[f(B^*_j) - 2\varepsilon]} + \sum_{j \in \overline{Z}}{[T_j - 2\varepsilon]}\\
            & \geq \sum_{j \in Z} {[f(B^*_j) - 2\varepsilon]} + \sum_{j \in \overline{Z}}{[f(B^*_j) - 2\varepsilon]}\\
            & = \sum_j {[f(B^*_j) - 2\varepsilon]} = f(\OPT_P) - 2K\varepsilon.\\
    \end{align*}

    A primeira desigualdade vale pela definição de~$m$. A segunda desigualdade vale pelo fato mostrado acima. Já a última desigualdade vale pois~${T_j}$ é um limite superior para~$f(B^*_j)$. O custo da solução devolvida é

    \begin{align*}
        f(S)
            & = f(S_P) + f(S_G) \\
            & = f(S_P) + f(\OPT_G)\\
            & \geq f(\OPT_P) - 2K\varepsilon + f(\OPT_G)\\
            & \geq f(\OPT) - 2K\varepsilon. \\
    \end{align*}

    Para qualquer~${\varepsilon' > 0}$, fazendo~${\varepsilon = \varepsilon'/2K}$, a solução obtida tem preenchimento de pelo menos~${(1-\varepsilon') f(\OPT)}$. Dessa forma, o Algoritmo~\ref{alg:7} é um PTAS para o MAXSPACE-R.
\end{proof}