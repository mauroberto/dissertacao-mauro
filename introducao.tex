% !TeX root = main.tex

Com o crescimento das mídias sociais, como Facebook, Twitter e LinkedIn, ocorreu um grande impulso para a propaganda na internet. Por exemplo, no ano de 2013 foram gastos cerca de~US\$5{,}1 bilhões em propaganda em redes sociais e o valor estimado para o ano de 2018 foi de aproximadamente~US\$15 bilhões de dólares. O Facebook arrecadou, sozinho,~US\$2{,}96 bilhões em propagandas no terceiro trimestre de 2014~\cite{kumar2015optimization}.

De forma geral, o valor arrecadado em propagandas na internet cresceu muito no século~XXI. Em 2013 o valor total arrecadado foi de~US\$42{,}78 bilhões, um crescimento de~17\% em relação ao ano anterior. Estima-se que o gasto em  propagandas na internet no ano de 2016 tenha alcançado~35\% de todo o gasto em propaganda, incluindo propagandas para a televisão~\cite{kumar2015optimization}.  Em 2016, anúncios em \textit{banners} representaram~31,4\% dos anúncios de internet~(considerando também \textit{banners} para plataformas móveis), o que representa um valor de~US\$22{,}7 bilhões de dólares~\cite{silverman2010iab}.

Muitos sites (como Google, Yahoo!~e Facebook) disponibilizam serviços de forma gratuita, enquanto apresentam propagandas aos usuários.~Para esses sites, as propagandas são a principal fonte de receita. Assim, é importante encontrar a melhor maneira de dispor as propagandas no espaço disponível, com o objetivo de maximizar o valor arrecadado~\cite{kumar2015optimization}.

O crescimento na exibição de anúncios na internet criou uma indústria multibilionária. Com isso, algoritmos para problemas de disposição de propagandas têm uma grande importância prática, pois é necessário achar boas soluções de forma rápida.

%Em \textit{sites} como o Google, os anúncios possuem um \textbf{Custo Por Clique} (CPC), que é o valor cobrado do anunciante quando seu anúncio é clicado por um usuário. Outra forma de receita, usada em \textit{sites} como o Facebook, é o \textbf{Custo Por Milhar} (CPM), que consiste do valor cobrado para cada mil exibições do anúncios. Também podemos citar o \textbf{Custo Por Ação} (CPA), que é o valor cobrado quando uma determinada ação é realizada sobre um anúncio. Por exemplo, quando uma loja anuncia um produto em um \textit{site} como o Google e um usuário clica nesse anúncio e realiza uma compra no \textit{site} do anunciante, o Google recebe um valor referente à ação de compra.

Consideramos a classe de problemas de disposição de propagandas introduzida por \citeauthor{adler2002scheduling}~\cite{adler2002scheduling}, em que, dado um conjunto~${\cala = \{A_1, A_2, \dots, A_n\}}$ de~$n$ propagandas, o objetivo é dispor um subconjunto~${\cala' \subseteq \cala}$ em um \textit{banner} em~$K$ intervalos de tempo iguais, chamados \textit{slots}. O conjunto de propagandas dispostas em um determinado \textit{slot}~$j$,~${1 \le j \le K}$, é representado por~${B_j \subseteq \cala'}$. Cada propaganda~$A_i$ tem um \textit{tamanho}~$s_i$ e uma \textit{frequência}~${w_i \leq K}$. O tamanho~$s_i$ representa o espaço que~$A_i$ ocupa em um \textit{slot} e a frequência~$w_i$ representa o número de \textit{slots} que devem conter uma cópia de~$A_i$. Uma propaganda~$A_i$ pode aparecer no máximo uma vez por \textit{slot} e~$A_i$ é dita \textit{disposta} se~$w_i$ cópias de~$A_i$ aparece em \textit{slots} com no máximo uma cópia por \textit{slot}~\cite{adler2002scheduling, dawande2003performance}.

Os principais problemas dessa classe são o MINSPACE e o MAXSPACE. No MINSPACE, todas as propagandas devem ser dispostas nos \textit{slots} e o objetivo é minimizar a altura do \textit{slot} mais alto, em que a altura de cada \textit{slot}~$j$ é dada por~$\sum_{A_i \in B_j}{s_i}$. No MAXSPACE, um limite superior~$L$ é especificado para a altura de cada \textit{slot}. Uma solução viável para o MAXSPACE consiste em um subconjunto~${\cala' \subseteq \cala}$ de forma que toda propaganda~${A_i \in \cala'}$ esteja disposta e nenhum \textit{slot} tenha excedido a altura máxima~$L$, isto é, para todo \textit{slot}~$B_j$,~${\sum_{A_i \in B_j}{s_i}\leq L}$. O objetivo do MAXSPACE é maximizar a receita, dada por~$\sum_{A_i \in \cala'}{s_i \cdot w_i}$, ou seja, o valor associado à propaganda é a sua altura multiplicada pela sua frequência. O MINSPACE e o MAXSPACE são fortemente NP-difíceis~\cite{adler2002scheduling, dawande2003performance}.

Considere as propagandas descritas na Tabela~\ref{tab:prop}.

\begin{table}[H]
\centering
\begin{tabular}{c|ccccccc}
\hline
\textbf{$A_i$} & $A_1$ & $A_2$ & $A_3$ & $A_4$ & $A_5$ & $A_6$ & $A_7$ \\ \hline
\textbf{$s_i$} & 6     & 4     & 2     & 3     & 1     & 1     & 5     \\
\textbf{$w_i$} & 3     & 2     & 1     & 2     & 1     & 1     & 1     \\ \hline
\end{tabular}
\caption{Exemplos de Propagandas.}
\label{tab:prop}
\end{table}

Nas Figuras~\ref{fig:min} e~\ref{fig:max} são apresentados exemplos de soluções para o MINSPACE e o MAXSPACE, respectivamente, usando as propagandas da Tabela~\ref{tab:prop}.

\begin{figure}[H]
  \centering
      \includegraphics[width=.75\textwidth]{minspace.png}
  \caption[Exemplos de soluções para o MINSPACE.]{Exemplos de soluções para o MINSPACE usando as propagandas da Tabela~\ref{tab:prop} e considerando~${K = 4}$. Em~(a) temos uma solução viável com valor~$11$. Em~(b) temos uma solução ótima com valor~$10$~\cite{dawande2003performance}.}
  \label{fig:min}
\end{figure}

\begin{figure}[H]
  \centering
      \includegraphics[width=.75\textwidth]{maxspace_exemplo.png}
  \caption[Exemplos de soluções para o MAXSPACE.]{Exemplos de soluções para o MAXSPACE usando as propagandas da Tabela~\ref{tab:prop} com~${L = 6}$ e~${K = 4}$. Em~(a) temos uma solução viável. Em~(b) temos uma solução ótima~\cite{dawande2003performance}.}
  \label{fig:max}
\end{figure}

Neste trabalho, abordamos o problema original MAXSPACE, além de algumas variantes propostas, do ponto de vista de heurísticas e algoritmos de aproximação.

Na Seção~\ref{sec:var} apresentamos as variantes propostas neste trabalho. No Capítulo~\ref{sec:pre} introduzimos os conceitos utilizados no restante do texto. No Capítulo~\ref{sec:revisao} apresentamos os trabalhos existentes na literatura que abordam os problemas MAXSPACE e MINSPACE. Nos Capítulos~\ref{sec:aprox} e~\ref{sec:heuristicas} apresentamos, respectivamente, os algoritmos de aproximação e as heurísticas desenvolvidos durante o mestrado. No Capítulo~\ref{sec:comp} apresentamos como foram realizados os experimentos computacionais das heurísticas e os resultados obtidos. Por fim, discutimos os resultados e trabalhos futuros no Capítulo~\ref{sec:conclusao}.

\section{Variantes}
\label{sec:var}

O MAXSPACE considera que o valor de uma propaganda é dada pela altura multiplicada pelo número de \textit{slots}  (ou unidades de tempo) em que ela aparece. Na prática, o valor de uma propaganda pode ser influenciado por outros fatores, como o número de cliques (ou a esperança do número de cliques) que a propaganda gera para o anunciante~\cite{briggs1997advertising}. Definimos MAXSPACE-V como a variante do MAXSPACE em que cada propaganda~$A_i$ possui um valor~$v_i$ que pode não estar relacionado com~$s_iw_i$.

A unidade de tempo relativa a cada \textit{slot} pode representar segundos, minutos ou perídios maiores, como dias e semanas. Muitas vezes, podemos considerar a ideia de \textit{deadlines} e \textit{release dates}. Uma propaganda pode possuir um \textit{deadline}, que indica até qual período ela pode aparecer. Por exemplo, propagandas para o Natal devem ser exibidas até dia 24 de dezembro. De forma análoga, uma propaganda também pode possuir um \textit{release date}, que indica a data a partir da qual a propaganda pode ser exibida.

Propusemos então as variantes MAXSPACE-D e MAXSPACE-R que, respectivamente, adicionam \textit{deadlines} e \textit{release dates}. No MAXSPACE-D, cada propaganda~$A_i$ possui um \textit{deadline}~${d_i \leq K}$ que indica até qual \textit{slot}~$A_i$ pode aparecer. De forma análoga, no MAXSPACE-R, cada propaganda~$A_i$ possui um \textit{release date}~${r_i \geq 1}$, que indica a partir de qual \textit{slot}~$A_i$ pode aparecer.

Outra variante que pode ser interessante na prática é a que considera que cada propaganda possui um orçamento (ao invés de uma frequência) e que esse orçamento é reduzido quando a propaganda é exibida (considerando a probabilidade de clique, por exemplo). Para formalizar essa ideia de frequência variável propusemos a variante MAXSPACE-W, em que cada propaganda~$A_i$ possui um intervalo para a frequência que varia entre~$w^{min}_{i}$ e~$w^{max}_{i}$.

Propusemos as variantes MAXSPACE-RD e MAXSPACE-RDWV a partir da junção das variantes propostas anteriormente. O MAXSPACE-RD é a variante que possui \textit{release dates} e \textit{deadlines} e o MAXSPACE-RDWV é a variante que possui \textit{release dates}, \textit{deadlines}, frequência variável e valor generalizado.

Na Figura~\ref{fig:var} são exibidas as variantes propostas e quais problemas cada variante generaliza. Um problema é definido generalizando os problemas indicados pelas setas; todo problema generaliza MAXSPACE.

\begin{figure}[H]
  \centering
  \includegraphics[width=.5\textwidth]{variantes.png}
  \caption[Variantes propostas.]{Variantes propostas. As setas indicam quais problemas cada variante generaliza.}
  \label{fig:var}
\end{figure}

\section{Nossos Resultados}

Neste trabalho, abordamos o problema MAXSPACE e as variantes propostas com heurísticas e com algoritmos de aproximação.

Desenvolvemos algoritmos baseados nas meta-heurísticas \textit{Greedy Randomized Adaptive Search Procedure}~(GRASP), \textit{Variable Neighborhood Search}~(VNS), \textit{Variable Neighborhood Descent}~(VND), Busca Local e Busca Tabu para o MAXSPACE e para o MAXSPACE-RDWV. Os resultados obtidos nos testes computacionais foram comparados com o algoritmo genético híbrido~Hybrid-GA proposto por~\citeauthor{kumar2006scheduling}~\cite{kumar2006scheduling}. Também adaptamos o algoritmo Hybrid-GA para o MAXSPACE-RDWV e comparamos com as nossas meta-heurísticas para esse problema. Algumas das heurísticas desenvolvidas, como o GRASP, obtiveram resultados melhores que o Hybrid-GA nos testes computacionais realizados em ambos os problemas.

Considerando algoritmos de aproximação, abordamos o problema MAXSPACE-R e apresentamos um algoritmo ótimo para propagandas grandes (que possuem~${s_i > L/2}$), uma~$1/4$-aproximação para propagandas médias (que possuem~${L/4 \leq s_i \leq L/2}$) e uma~$1/4$-aproxi\-mação para propagandas pequenas (que possuem~${s_i \leq L/4}$). Usando esses algoritmos apresentamos uma~$1/9$-aproximação para o problema que permite propagandas de todos os tamanhos. Também apresentamos um esquema de aproximação em tempo polinomial~(PTAS) para o problema MAXSPACE-RD com número de \textit{slots} constantes. A melhor aproximação para o MAXSPACE existente na literatura possui fator~${(1/3 - \varepsilon)}$ e foi apresentada por~\citeauthor{freund2002approximating}~\cite{freund2002approximating}.

%\lehi{acho que você poderia destacar um pouco mais os seus resultados  aqui; e.g., como eles se comparam com o que existia antes/existe? há algo que pode ser levado a outros problemas, etc? um bom lugar para discutir isso tb é no capítulo de conclusão se preferir deixar toda discussão concentrada em um só lugar}